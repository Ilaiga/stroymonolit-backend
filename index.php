<?php
require __DIR__ . '/protected/vendor/autoload.php';

defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

error_reporting(APPLICATION_ENV == 'dev' ? E_ALL : 0);
//error_reporting(E_ALL);

// change the following paths if necessary
$yii=dirname(__FILE__).'/protected/vendor/yiisoft/yii/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

if (APPLICATION_ENV == 'dev') {
	defined('YII_DEBUG') or define('YII_DEBUG',true);
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
}

require_once($yii);

Yii::createWebApplication($config)->run();

