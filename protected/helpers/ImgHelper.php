<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 30.04.2019
 * Time: 12:57
 */

class ImgHelper
{
	public static function img($url, $classes = "", $href = false, $style = "", $alt = "")
	{
		$webp = file_exists(Yii::app()->basePath.'/../'.$url.'.webp') ? $url.'.webp' : $url;
		$hrefurl = $href ? "href='$url'" : '';
		echo "<picture alt=\"$alt\">
			<source alt=\"$alt\" class=\"lazyload $classes\" data-srcset=\"$webp\" type=\"image/webp\">
			<img alt=\"$alt\" $hrefurl class=\"lazyload $classes\" data-src=\"$url\" style=\"$style\">
		</picture>";
	}
}