<?php

class ArticlesController extends AController
{
	public function actionIndex() {
		$articles = new Articles('search');

		$this->render('index', [
			'articles' => $articles,
			'provider' => $articles->allSearch()
		]);
	}

	public function actionCreate() {
		$article = new Articles();
		$categories = (new ArticlesCategories())->listData();

		if (Yii::app()->request->isPostRequest) {
			$article->setAttributes(Yii::app()->request->getPost('Articles'));
			$article->image = CUploadedFile::getInstance($article, 'image');
			$article->validate();
			
			if ($article->hasErrors() == false) {
				$article->save($runValidation = false);
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				Yii::app()->controller->redirect(['update', 'id' => $article->articleID]);
			}
		}

		$this->render('create', [
			'article' => $article,
			'categories' => $categories
		]);
	}

	public function actionUpdate() {
		$article = $this->loadRecord('Articles');
		$categories = (new ArticlesCategories())->listData();
		
		if (Yii::app()->request->isPostRequest) {
			$article->setAttributes(Yii::app()->request->getPost('Articles'));
			$article->image = CUploadedFile::getInstance($article, 'image');
			$article->validate();

			if ($article->hasErrors() == false) {
				$article->save($runValidation = false);
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				Yii::app()->controller->refresh();
			} else {
				Yii::app()->user->setFlash('failed', true);
			}
		}

		$this->render('update', [
			'article' => $article,
			'categories' => $categories
		]);
	}

	public function actionDelete() {
		$article = $this->loadRecord('Articles');
		$article->delete();
		Yii::app()->controller->redirect(Yii::app()->request->urlReferrer);
	}

	public function actionDeleteImage() {
		$article = $this->loadRecord('Articles');
		$article->deleteImages();
	}

	public function filters()
	{
		return ['accessControl'];
	}

	public function accessRules()
	{
		return [
			['allow', 'roles' => ['admin']],
			['deny', 'users'=> ['*']]
		];
	}

}