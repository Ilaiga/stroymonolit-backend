<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 18.04.2019
 * Time: 14:20
 */

class ServicesController extends AController
{
	protected $templates = [
		'1' => 'service_form',
		'35' => 'brand_form'
	];

	public function actionIndex()
	{
		$model = Services::model()->roots()->findAll();
		$this->render('index', ['services' => $model]);
	}

	public function actionAdd($root = null)
	{
		$error = false;
		$model = new Services();
		if(!empty($root)) {
			$root = Services::model()->findByPk($root);
			$model->parentUrl = $this->getParentUrl($root);
		}
		if(Yii::app()->request->getPost('Services')) {
			$model->attributes = Yii::app()->request->getPost('Services');
			$model->image = CUploadedFile::getInstance($model, 'image');
			$model->pImage = CUploadedFile::getInstance($model, 'pImage');
			if(!$root) {
				if($model->saveNode()) {
					Yii::app()->user->setFlash('success', 'Данные добавлены');
					$this->redirect($this->createUrl('/admin/services'));
				}
			} else {
				$priceModels = CHtml::listData($model->priceItems, 'id', function ($el) {return $el;});
				if(($prices = Yii::app()->request->getPost('ServicePricesTable'))) {
					foreach ($prices as $i => $data) {
						$price = new ServicePricesTable();
						$price->serviceId = 1;
						$priceModels[] = $price;
						$price->setAttributes($data);
						$price->validate();
						if ($price->hasErrors()) $error = true;
					}
					$model->priceItems = $priceModels;
				}
				$sellPriceModels = CHtml::listData($model->sellPriceItems, 'id', function ($el) {return $el;});
				if(($prices = Yii::app()->request->getPost('ServiceSellPricesTable'))) {
					foreach ($prices as $i => $data) {
						$price = new ServiceSellPricesTable();
						$price->serviceId = 1;
						$sellPriceModels[] = $price;
						$price->setAttributes($data);
						$price->validate();
						if ($price->hasErrors()) $error = true;
					}
					$model->sellPriceItems = $sellPriceModels;
				}
				$model->validate();
				if(!$error) {
					if($model->appendTo($root)) {
						foreach ($priceModels as $price) {
							$price->serviceId = $model->id;
							$price->save(false);
						}
						foreach ($sellPriceModels as $price) {
							$price->serviceId = $model->id;
							$price->save(false);
						}
						Yii::app()->user->setFlash('success', 'Данные добавлены');
						$this->redirect($this->createUrl('/admin/services'));
					}

				}
			}

		}
		$this->render($this->templates[$root->root], ['service' => $model, 'title' => 'Новая услуга']);
	}

	public function actionEdit($root)
	{
		$error = false;
		$model = Services::model()->findByPk($root);
		if($model->level != 1)
			$model->parentUrl = $this->getParentUrl($model->parent()->find());
		if(Yii::app()->request->getPost('Services')) {
			$priceModels = CHtml::listData($model->priceItems, 'id', function ($el) {return $el;});
			$prices1 = Yii::app()->request->getPost('ServicePricesTable') ?? [];
			$data1 = CHtml::listData($prices1 ?? [], 'id', function ($el) {return $el;});
			foreach ($priceModels as $item) {
				if(empty($data1[$item->id])) ServicePricesTable::model()->deleteByPk($item->id);
			}
			foreach ($prices1 as $i => $data1) {
				if(!empty($data1['id'])) {
					$price = $priceModels[$data1['id']];
				} else {
					$price = new ServicePricesTable();
					$price->serviceId = $model->id;
					$priceModels[] = $price;
				}
				$price->setAttributes($data1);
				$price->validate();
				if (!$price->hasErrors()) {

					$price->save(false);
				}else {
					$error = true;
				}
			}

			$sellPriceModels = CHtml::listData($model->sellPriceItems, 'id', function ($el) {return $el;});
			$prices2 = Yii::app()->request->getPost('ServiceSellPricesTable') ?? [];
			$data = CHtml::listData($prices2 ?? [], 'id', function ($el) {return $el;});
			foreach ($sellPriceModels as $item) {
				if(empty($data[$item->id])) ServiceSellPricesTable::model()->deleteByPk($item->id);
			}
			foreach ($prices2 as $i => $data) {
				if(!empty($data['id'])) {
					$price = $sellPriceModels[$data['id']];
				} else {
					$price = new ServiceSellPricesTable();
					$price->serviceId = $model->id;
					$sellPriceModels[] = $price;
				}
				$price->setAttributes($data);
				$price->validate();
				if (!$price->hasErrors()) {
					$price->save(false);
				}else {
					$error = true;
				}
			}


			$model->priceItems = $priceModels;
			$model->sellPriceItems = $sellPriceModels;
			$model->attributes = Yii::app()->request->getPost('Services');
			$model->image = CUploadedFile::getInstance($model, 'image');
			$model->pImage = CUploadedFile::getInstance($model, 'pImage');
			if($model->saveNode() && !$error) {
				Yii::app()->user->setFlash('success', 'Данные обновлены');
				$this->redirect($this->createUrl('/admin/services'));
			}
		}
		$this->render($this->templates[$model->root], ['service' => $model, 'title' => 'Редактирование услуги']);
	}

	public function actionDelete($root)
	{
		$model = Services::model()->findByPk($root);
		if($model->children()->find()){
			Yii::app()->user->setFlash('error', 'Невозможно удалить элемент имеющий дочерние блоки.');
			$this->redirect($this->createUrl('/admin/services/'));
		}
		try {
//			if($model->getImagePath())
//				unlink($model->getImagePath());
			Services::model()->deleteByPk($root);
			Yii::app()->user->setFlash('success', 'Элемент удален');
		} catch (Exception $e) {
			Yii::app()->user->setFlash('error', $e->getMessage());
		}
		$this->redirect($this->createUrl('/admin/services/'));
	}

	public function actionChangePosition()
	{
		$request = Yii::app()->request->getPost('pos');
		$element = Services::model()->findByPk($request['id']);
		$target = Services::model()->findByPk($request['targetId']);
		switch ($request['action']) {
			case 'before':
				$element->moveBefore($target);
				break;
			case 'after':
				$element->moveAfter($target);
				break;
			case 'append':
				$element->moveAsFirst($target);
				break;
		}
	}

	private function getParentUrl(Services $service)
	{
		$ancestors = $service->ancestors()->findAll();
		$url = [];
		foreach ($ancestors as $parentService) {
			$url[] = $parentService->slug;
		}
		$url[] = $service->slug;
		if(count($url) == 1) return '/';
		array_shift($url);
		return '/' . implode('/', $url) . '/';
	}

	public function filters()
	{
		return ['accessControl'];
	}

	public function accessRules()
	{
		return [
			['allow', 'roles' => ['admin']],
			['deny', 'users'=> ['*']]
		];
	}
}