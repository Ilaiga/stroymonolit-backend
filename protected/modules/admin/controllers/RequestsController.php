<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 24.04.2019
 * Time: 11:20
 */

class RequestsController extends AController
{
	public function actionIndex()
	{
		$data = RequestForm::model()->findAllByAttributes([], ['order' => 'id desc']);
		$result = [];
		CHtml::listData($data, 'type', function ($item) use (&$result) {
			$result[$item->type][] = $item;
		});
		$this->render('index', ['data' => $result]);
	}

	public function actionDelete()
	{
		RequestForm::model()->findByPk(Yii::app()->request->getParam('id'))->delete();
	}

	public function filters()
	{
		return ['accessControl'];
	}

	public function accessRules()
	{
		return [
			['allow', 'roles' => ['admin']],
			['deny', 'users'=> ['*']]
		];
	}
}