<?php

class PagesController extends AController
{
	public function actionIndex()
	{
		$model = Pages::model()->roots()->findAll();
		$this->render('index', ['pages' => $model]);
	}

	public function actionAdd($root = null)
	{
		$model = new Pages();
		if(!empty($root)) {
			$root = Pages::model()->findByPk($root);
			$model->parentUrl = $this->getParentUrl($root);
		}
		if(Yii::app()->request->getPost('Pages')) {
			$model->attributes = Yii::app()->request->getPost('Pages');
			$model->iconFile = CUploadedFile::getInstance($model,'iconFile');
			if(!$root) {
				if($model->saveNode()) {
					Yii::app()->user->setFlash('success', 'Данные добавлены');
					$this->redirect($this->createUrl('/admin/pages'));
				}
			} else {
				if($model->appendTo($root)) {
					Yii::app()->user->setFlash('success', 'Данные добавлены');
					$this->redirect($this->createUrl('/admin/pages'));
				}
			}

		}
		$this->render('add', ['page' => $model, 'templates' => PageTemplates::model()->findAll(), 'title' => 'Новая страница']);
	}

	public function actionEdit($root)
	{
		$model = Pages::model()->findByPk($root);
			if($root != 1)
		$model->parentUrl = $this->getParentUrl($model->parent()->find());
		if(Yii::app()->request->getPost('Pages')) {
			$model->attributes = Yii::app()->request->getPost('Pages');
			$model->iconFile = CUploadedFile::getInstance($model,'iconFile');
				if($model->saveNode()) {
					Yii::app()->user->setFlash('success', 'Данные обновлены');
					$this->redirect($this->createUrl('/admin/pages'));
				}
		}
		$this->render('add', ['page' => $model, 'templates' => PageTemplates::model()->findAll(), 'title' => 'Редактирование страницы']);
	}

	public function actionDelete($root)
	{
		$model = Pages::model()->findByPk($root);
		if($model->children()->find()){
			Yii::app()->user->setFlash('error', 'Невозможно удалить элемент имеющий дочерние блоки.');
			$this->redirect($this->createUrl('/admin/pages/'));
		}
		try {
			if($model->getImagePath())
				unlink($model->getImagePath());
			Pages::model()->deleteByPk($root);
			Yii::app()->user->setFlash('success', 'Элемент удален');
		} catch (Exception $e) {
			Yii::app()->user->setFlash('error', $e->getMessage());
		}
		$this->redirect($this->createUrl('/admin/pages/'));
	}

	public function actionChangePosition()
	{
		$request = Yii::app()->request->getPost('pos');
		$element = Pages::model()->findByPk($request['id']);
		$target = Pages::model()->findByPk($request['targetId']);
		switch ($request['action']) {
			case 'before':
				$element->moveBefore($target);
			break;
			case 'after':
				$element->moveAfter($target);
			break;
			case 'append':
				$element->moveAsFirst($target);
			break;
		}
	}

	private function getParentUrl(Pages $page)
	{
		$ancestors = $page->ancestors()->findAll();
		$url = [];
		foreach ($ancestors as $parentPage) {
			$url[] = $parentPage->slug;
		}
		$url[] = $page->slug;
		if(count($url) == 1) return '/';
		array_shift($url);
		return '/' . implode('/', $url) . '/';
	}

	public function filters()
	{
		return ['accessControl'];
	}

	public function accessRules()
	{
		return [
			['allow', 'roles' => ['admin']],
			['deny', 'users'=> ['*']]
		];
	}

}