<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 23.04.2019
 * Time: 16:27
 */

class MenuHelperComponent extends CApplicationComponent
{
	public $useCache = true;
	public $parent = 0;
	protected $requestUrl;

	public function __construct()
	{
		$this->requestUrl = Yii::app()->request->requestUri;
	}

	public function getChildPagesList($parent)
	{
		$category=Pages::model()->findByAttributes(['id' => $parent]);
		foreach ($category->children()->findAllByAttributes(['active' => 1], ['order' => 'lft asc']) as $item) {
			$result[] = [
				'name' => $item->title,
				'text' => $item->title,
				'url' => $item->url == $this->requestUrl ? 'javascript:;' : $item->url,
				'link' => $item->url == $this->requestUrl ? 'javascript:;' : $item->url,
				'class' => $item->url == $this->requestUrl ? '_active' : '',
				'slug' => $item->slug,
			];
		}
		return $result ?? [];
	}

	public function getChildServiceList($parent)
	{
		$category = Services::model()->findByAttributes(['id' => $parent]);
		if (!$category) return [];
		foreach ($category->children()->findAllByAttributes(['active' => 1, 'dontShowOnMenu' => 0], ['order' => 'lft asc']) as $item) {
			$result[] = [
				'name' => $item->menuTitle,
				'text' => $item->menuTitle,
				'url' => $item->url == $this->requestUrl ? 'javascript:;' : $item->url,
				'link' => $item->url == $this->requestUrl ? 'javascript:;' : $item->url,
				'class' => $item->url == $this->requestUrl ? '_active' : '',
				'slug' => $item->slug,
				'subList' => $this->getChildServiceList($item->id)
			];
		}
		return $result ?? [];
	}

}