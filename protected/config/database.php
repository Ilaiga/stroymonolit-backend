<?php
return [
	'dev' => [
		'mysql' => [
			'connectionString' => 'mysql:host=localhost;dbname=stroymonolit',
			'emulatePrepare' => true,
			'username' => 'stroymonolit',
			'password' => '20Passwd02x',
			'charset' => 'utf8',
		]
	],
	'prod' => [
		'mysql' => [
			'connectionString' => 'mysql:host=localhost;dbname=infodmrh_stroymo',
			'emulatePrepare' => true,
			'username' => 'infodmrh_stroymo',
			'password' => '20Passwd02x',
			'charset' => 'utf8',
		]
	]
];