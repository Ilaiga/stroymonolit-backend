<?php
return [
	'articles/<slug:[-_a-zA-Z0-9]+>' => 'articles/view',
	'articles' => 'articles/index',

	'sitemap.xml' => 'sitemap/index',

	['class' => 'application.components.ServicesUrlRule'],
	['class' => 'application.components.PagesUrlRule'],

	'<controller:(page)>/view/<pageId:\d+>' => '<controller>/view',
	'<controller:(page)>/link/<slug:[-_a-zA-Z0-9]+>' => '<slug>',
	'<controller:(service)>/view/<pageId:\d+>' => '<controller>/view',

	'<module:(admin)>/pages/<action:\w+>/<root:\d+>/' => '<module>/pages/<action>',
	'<module:(admin)>' => '<module>/default/index',
	'<module:(admin)>/<controller:\w+>/<action:\w+>/<id:\d+>/' => '<module>/<controller>/<action>',
	'<module:(admin)>/<controller:\w+>' => '<module>/<controller>/index',

	'admin/cp' => 'admin/config',
	'admin/cp/<groupId:\d+>' => 'admin/config',

	'admin' => 'admin/default/index',

	'<controller:\w+>' => '<controller>/index',
	'<controller:\w+>/<id:\d+>' => '<controller>/view',
	'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
	'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
];