<?php
$db = require(dirname(__FILE__) . '/' . 'database.php');
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import' => array(
		'application.models.*',
		'application.components.*',
		'ext.giix.components.*',
		'application.extensions.*',
		'ext.easyimage.EasyImage',
		'ext.eauth.services.*',
	),

	// application components
	'components'=>array(
		'request' => array(
			'hostInfo' => 'https://hotelsi.com',
			'baseUrl' => '',
			'scriptUrl' => '',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
		'urlManager' => array(
			'urlFormat' => 'path',
			'urlSuffix' => '',
			'showScriptName' => false,
			'appendParams' => false,
			'rules' => require(dirname(__FILE__) . '/url.rules.php'),
		),
		'settings' => ['class' => 'application.modules.admin.components.SettingsComponent', 'useCache' => true],
		'db' => APPLICATION_ENV == 'dev' ? $db['dev']['mysql'] : $db['prod']['mysql'],
	),
);
