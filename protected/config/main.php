<?php
$db = require(dirname(__FILE__) . '/' . 'database.php');
return array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name' => 'Condipack',
	'language' => 'ru',
	'charset'=>'utf-8',
	// preloading 'log' component
	'preload' => array('log'),

	// autoloading model and component classes
	'import' => array(
		'application.models.*',
		'application.models.forms.*',
		'application.components.*',
		'application.extensions.*',
		'application.helpers.*',
		'ext.giix.components.*',
		'ext.easyimage.EasyImage',
		'ext.eauth.services.*',
	),

	'modules' => array(
		'gii' => array(
			'class' => 'system.gii.GiiModule',
			'password' => 'trustno1',
			'ipFilters' => array('10.0.2.2', '::1'),
			'generatorPaths' => array(
			'ext.giix.generators',
			),
		),
		'office',
		'admin',
	),
	// application components
	'components'=>array(
		'cache' => array(
			'class' => 'system.caching.CFileCache',
		),
		'user' => array(
			'class' => 'WebUser',
			'allowAutoLogin' => true,
			'loginUrl' => '/admin/default/login',
		),
		'authManager'=>array(
			'class' => 'PhpAuthManager',
			'defaultRoles' => ['guest'],
		),
		'urlManager' => array(
			'urlFormat' => 'path',
			'urlSuffix' => '',
			'showScriptName' => false,
			'appendParams' => false,
			'rules' => require(dirname(__FILE__) . '/url.rules.php'),
		),
		'errorHandler' => array(
			'errorAction' => YII_DEBUG ? null : 'site/error',
		),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
		'viewRenderer' => array(
			'class' => 'ext.ETwigViewRenderer',
			// All parameters below are optional, change them to your needs
			'fileExtension' => '.twig',
			'options' => array(
				'autoescape' => true,
				'debug' => true,
			),
			'extensions' => [
				'Twig_Extension_Debug'
			],
			'globals' => array(
				'html' => 'Html',
				'ImgHelper' => 'ImgHelper',
				'app' => Yii::app(),
				'yii' => new Yii,
			),
			'functions' => array(
				'rot13' => 'str_rot13',
				'filemtime' => 'filemtime',
				'strip' => 'strip_tags'
			),
			'filters' => array(
				'jencode' => 'CJSON::encode',
				'dump' => 'print_r'
			),
		),
		'easyImage' => array(
			'class' => 'application.extensions.easyimage.EasyImage',
			'driver' => 'GD',
			'quality' => 100,
			'retinaSupport' => false
		),
		'settings' => ['class' => 'application.modules.admin.components.SettingsComponent', 'useCache' => true],
		'openGraph' => ['class' => 'application.components.OpenGraphComponent'],
		'menuHelper' => ['class' => 'application.modules.admin.components.MenuHelperComponent', 'useCache' => true],
		'mailer' => array(
			'class' => 'application.extensions.PHPMailerAdapter',
		),
		'db' => APPLICATION_ENV == 'dev' ? $db['dev']['mysql'] : $db['prod']['mysql'],
	),
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params' => array(
		// this is used in contact page
		'adminEmail' => 'nsk.web.create@gmail.com',
	),
);
