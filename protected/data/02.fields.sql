ALTER TABLE `services` ADD COLUMN `halfMonthPrice` VARCHAR(50) NULL DEFAULT NULL AFTER `monthPrice`;
ALTER TABLE `services` ADD COLUMN `dontShowOnMenu` TINYINT(1) NULL DEFAULT NULL AFTER `showOnServicesBlock`;
ALTER TABLE `services` CHANGE COLUMN `dontShowOnMenu` `dontShowOnMenu` TINYINT(1) NULL DEFAULT '0' AFTER `showOnServicesBlock`;
UPDATE `services` SET `dontShowOnMenu` = 0 where 1=1
ALTER TABLE `services` ADD COLUMN `priceTableFields` TEXT NULL DEFAULT NULL AFTER `depositPrice`;