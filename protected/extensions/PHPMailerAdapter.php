<?php
Yii::import('application\\vendor\\phpmailer\\phpmailer\\class.phpmailer', true);

class PHPMailerAdapter  extends CApplicationComponent
{
    //***************************************************************************
    // Configuration
    //***************************************************************************

    /**
     * The path to the directory where the view for getView is stored. Must not
     * have ending dot.
     *
     * @var string
     */
    protected $pathViews = 'application.views.email';

    /**
     * The path to the directory where the layout for getView is stored. Must
     * not have ending dot.
     *
     * @var string
     */
    protected $pathLayouts = 'application.views.email.layouts';

    //***************************************************************************
    // Private properties
    //***************************************************************************

    /**
     * The internal PHPMailer object.
     *
     * @var object PHPMailer
     */
    private $_myMailer;

    public function __construct()
    {
        $this->_myMailer = new PHPMailer();
        $this->_myMailer->CharSet = 'utf-8';
    }


    //***************************************************************************
    // Setters and getters
    //***************************************************************************

    /**
     * Setter
     *
     * @param string $value pathLayouts
     * @throws CException
     */
    public function setPathLayouts($value)
    {
        if (!is_string($value) && !preg_match("/[a-z0-9\.]/i"))
            throw new CException(Yii::t('EMailer', 'pathLayouts must be a Yii alias path'));
        $this->pathLayouts = $value;
    }

    /**
     * Getter
     *
     * @return string pathLayouts
     */
    public function getPathLayouts()
    {
        return $this->pathLayouts;
    }

    /**
     * Setter
     *
     * @param string $value pathViews
     * @throws CException
     */
    public function setPathViews($value)
    {
        if (!is_string($value) && !preg_match("/[a-z0-9\.]/i"))
            throw new CException(Yii::t('EMailer', 'pathViews must be a Yii alias path'));
        $this->pathViews = $value;
    }

    /**
     * Getter
     *
     * @return string pathViews
     */
    public function getPathViews()
    {
        return $this->pathViews;
    }

    //***************************************************************************
    // Magic
    //***************************************************************************

    /**
     * Call a PHPMailer function
     *
     * @param string $method the method to call
     * @param array $params the parameters
     * @return mixed
     * @throws CException
     */
    public function __call($method, $params)
    {
        if (is_object($this->_myMailer) && get_class($this->_myMailer)==='PHPMailer') return call_user_func_array(array($this->_myMailer, $method), $params);
        else throw new CException(Yii::t('EMailer', 'Can not call a method of a non existent object'));
    }

    /**
     * Setter
     *
     * @param string $name the property name
     * @param string $value the property value
     * @throws CException
     */
    public function __set($name, $value)
    {
        if (is_object($this->_myMailer) && get_class($this->_myMailer)==='PHPMailer') $this->_myMailer->$name = $value;
        else throw new CException(Yii::t('EMailer', 'Can not set a property of a non existent object'));
    }

    /**
     * Getter
     *
     * @param string $name
     * @return mixed
     * @throws CException
     */
    public function __get($name)
    {
        if (is_object($this->_myMailer) && get_class($this->_myMailer)==='PHPMailer') return $this->_myMailer->$name;
        else throw new CException(Yii::t('EMailer', 'Can not access a property of a non existent object'));
    }

    //***************************************************************************
    // Utilities
    //***************************************************************************

    /**
     * Displays an e-mail in preview mode.
     *
     * @param string $view the class
     * @param array $vars
     * @param string $layout
     * @return string
     */
    public function getView($view, $vars = array(), $layout = null)
    {
        $body = Yii::app()->controller->renderPartial($this->pathViews.'.'.$view, array_merge($vars, array('content'=>$this->_myMailer)), true);
        if ($layout !== null) {
            $body = Yii::app()->controller->renderPartial($this->pathLayouts.'.'.$layout, array('content'=>$body), true);
        }
        return $body;
    }
}