<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 10/04/2019
 * Time: 14:01
 */

class GoogleRecaptcha
{
	const SECRET_KEY = '6Le5H7YUAAAAAOJVMZWEPPPgOUpe02YMGl28ZHp1';
	const PUBLIC_KEY = '6Le5H7YUAAAAAIuoxMyHgiAb5GxeFc55Pv1nh9xD';

	private $g_recaptcha_response = null;
	public function __construct($g_recaptcha_response)
	{
		$this->g_recaptcha_response = $g_recaptcha_response;
	}

	protected function validate() {

		if(empty($this->g_recaptcha_response)) {
			return false;
		}

		$url_to_google_api = "https://www.google.com/recaptcha/api/siteverify";
		$query = $url_to_google_api . '?secret=' . self::SECRET_KEY . '&response=' . $this->g_recaptcha_response . '&remoteip=' . $_SERVER['REMOTE_ADDR'];
		$data = json_decode(file_get_contents($query));
		if ($data->success) {
			return true;
		} else {
			return false;
		}
	}

	public function getResult() {
		return $this->validate();
	}



}