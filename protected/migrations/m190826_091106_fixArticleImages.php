<?php

class m190826_091106_fixArticleImages extends CDbMigration
{
	public function up()
	{
		$this->execute('update articles set text = REPLACE(text, \'http://csolutions.tmweb.ru:5005\', \'\') where 1;');
		$this->execute('update articles set text = REPLACE(text, \'http://188.225.81.132:81\', \'\') where 1;');
		$this->execute('update services set description = REPLACE(description, \'http://188.225.81.132:81\', \'\') where 1;');
		$this->execute('update services set description = REPLACE(description, \'http://csolutions.tmweb.ru:5005\', \'\') where 1;');
	}

	public function down()
	{
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}