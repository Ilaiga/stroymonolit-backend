<?php

class m190829_054916_phones extends CDbMigration
{
	public function up()
	{
		$this->execute('INSERT INTO `configParams` (`id`, `groupID`, `type`, `name`, `label`, `value`, `placeholder`, `dataProvider`) VALUES (13, 3, \'text\', \'phone\', \'Телефон (шапка)\', \'+7 (495) 723-88-80\', NULL, NULL);
INSERT INTO `configParams` (`id`, `groupID`, `type`, `name`, `label`, `value`, `placeholder`, `dataProvider`) VALUES (34, 3, \'text\', \'phoneBuyout\', \'Телефон (выкуп опалубки)\', \'+7 (925) 596-82-26\', NULL, NULL);
');
	}

	public function down()
	{
		echo "m190829_054916_phones does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}