<?php

class m190708_092905_update_db extends CDbMigration
{
	public function safeUp()
	{
		$source = getcwd() . '/data/01.schema.sql';
		if (file_exists($source)) {
			$this->execute(file_get_contents($source));
		}
	}

	public function down()
	{
		echo "m190708_092905_update_db does not support migration down.\n";
		return false;
	}

	/*
	// ALTER TABLE `services` ADD COLUMN `halfMonthPrice` VARCHAR(50) NULL DEFAULT NULL AFTER `monthPrice`;
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}