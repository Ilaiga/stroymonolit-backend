<?php

class m190823_075802_afterHeadDescr extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE `services`
	ADD COLUMN `shortDescriptionAfterHead` TEXT NOT NULL AFTER `requestFormTitle`;
');
	}

	public function down()
	{
		echo "m190823_075802_afterHeadDescr does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}