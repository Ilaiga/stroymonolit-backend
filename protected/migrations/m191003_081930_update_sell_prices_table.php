<?php

class m191003_081930_update_sell_prices_table extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE `serviceSellPricesTable`
	ALTER `newPrice` DROP DEFAULT,
	ALTER `usedPrice` DROP DEFAULT;
ALTER TABLE `serviceSellPricesTable`
	CHANGE COLUMN `newPrice` `newPrice` VARCHAR(50) NULL AFTER `name`,
	CHANGE COLUMN `usedPrice` `usedPrice` VARCHAR(50) NULL AFTER `newPrice`;
');
	}

	public function down()
	{
		echo "m191003_081930_update_sell_prices_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}