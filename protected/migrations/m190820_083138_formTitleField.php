<?php

class m190820_083138_formTitleField extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE `services`
	ADD COLUMN `requestFormTitle` TEXT NULL DEFAULT NULL AFTER `updatedAt`;
');
	}

	public function down()
	{
		echo "m190820_083138_formTitleField does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}