<?php

class m190708_122008_sell_prices_table extends CDbMigration
{
	public function safeUp()
	{
		$source = getcwd() . '/data/03.sellPrices.sql';
		if (file_exists($source)) {
			$this->execute(file_get_contents($source));
		}
	}

	public function down()
	{
		echo "m190708_092905_update_db does not support migration down.\n";
		return false;
	}
}