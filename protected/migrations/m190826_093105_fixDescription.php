<?php

class m190826_093105_fixDescription extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE `services`
	CHANGE COLUMN `shortDescriptionAfterHead` `shortDescriptionAfterHead` TEXT NULL COLLATE \'utf8_unicode_ci\' AFTER `requestFormTitle`;
');
	}

	public function down()
	{
		echo "m190826_093105_fixDescription does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}