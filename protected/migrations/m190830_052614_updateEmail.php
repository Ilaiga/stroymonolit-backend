<?php

class m190830_052614_updateEmail extends CDbMigration
{
	public function up()
	{
		$this->execute('update services set shortDescription = REPLACE(shortDescription, \'почту info@stroymonolit.su\', \'почту <a href="mailto:info@stroymonolit.su">info@stroymonolit.su</a>\') where 1 = 1');
	}

	public function down()
	{
		echo "m190830_052614_updateEmail does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}