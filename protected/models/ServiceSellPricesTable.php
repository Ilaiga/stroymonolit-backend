<?php

Yii::import('application.models._base.BaseServiceSellPricesTable');

class ServiceSellPricesTable extends BaseServiceSellPricesTable
{
	public $id;
	public $position;
	public $weight;
	public $newPrice;
	public $usedPrice;
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}