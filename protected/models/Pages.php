<?php

Yii::import('application.models._base.BasePages');

class Pages extends BasePages implements LastModified
{
	use LastModifiedModel;

	public $icon;
	public $iconFile;
	public $parentUrl;
	public $metaTitle;
	public $description;
	public $class;

	const IMAGES_DIR = '/images/icons/pages/';

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules()
	{
		return array_merge(parent::rules(), [
			['iconFile', 'file', 'types'=>'ico', 'allowEmpty' => true],
		]);
	}

	public function getNewImageName()
	{
		if($this->icon) {
			unlink($this->getImagePath());
		}
		return md5($this->iconFile->getName() . time()) . '.' . $this->iconFile->getExtensionName();
	}

	public function getImagePath()
	{
		if($this->icon)
			return Yii::getPathOfAlias('webroot') . self::IMAGES_DIR . $this->icon;
	}

	public function getImageUrl()
	{
		if($this->icon) {
			return self::IMAGES_DIR . $this->icon;
		}
	}

	public function behaviors()
	{
		return array(
			'nestedSetBehavior'=>array(
				'class'=> NestedSetBehavior::class,
				'leftAttribute'=>'lft',
				'rightAttribute'=>'rgt',
				'levelAttribute'=>'level',
			),
		);
	}

	public function attributeLabels()
	{
		return [
			'title' => Yii::t('pages', 'Заголовок'),
			'active' => Yii::t('pages', 'Активен'),
			'templateID' => Yii::t('pages', 'Шаблон'),
			'content' => Yii::t('pages', 'Контент'),
			'route' => Yii::t('pages', 'Route (маршрут к действию)'),
		];
	}

	public function getUrl()
	{
		$url[] = $this->slug;
		foreach ($this->ancestors()->findAll() as $item){
			if($item->slug != '/')
			$url[] = $item->slug;
		}
		$link = '/'.implode('/', array_reverse($url) ?? []);
		if(Yii::app()->request->requestUri == $link) {
			$this->class = '_active';
			return 'javascript:;';
		}
		return $link;
	}

	public function activeClass()
	{
		return $this->class;
	}

	protected function afterValidate()
	{
		if($this->iconFile) {
			$this->icon = $this->getNewImageName();
			$this->iconFile->saveAs($this->getImagePath());
		}
		parent::afterValidate();
	}

	public function getSubList($excude = [])
	{
		$exQ = $excude ? 'and id not in('.implode(',', $excude).')' : '';
		return $this->children()->findAll(new CDbCriteria(['condition' => 'active = 1 '.$exQ]));
	}

	public static function siteMap(\Tackk\Cartographer\Sitemap $sitemap, $freq = null, $priority = null)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'active = 1';
		$criteria->order = 'updatedAt desc';
		foreach (self::model()->findAll($criteria) as $model) {
			if($model->getUrl() != '//'){
				$sitemap->add(Yii::app()->controller->createAbsoluteUrl($model->getUrl()), $model->updatedAt, $freq, $priority);
			}
		}
	}
}