<?php

Yii::import('application.models._base.BaseDocuments');

class Documents extends BaseDocuments implements LastModified
{
	use LastModifiedModel;

	const FILE_DIRECTORY = '/files/';
	const FILE_MAX_SIZE = 5242880;

	public $fileTmp;
	public $name;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return array_merge(parent::rules(), [
			['fileTmp', 'file', 'types' => 'txt, doc, docx, pdf, xls, xlsx', 'allowEmpty' => true, 'maxSize' => self::FILE_MAX_SIZE],
		]);
	}

	private function newFile($ext) {
		return uniqid() . '.' . $ext;
	}

	public function getFile() {
		if ($this->fileName) {
			return self::FILE_DIRECTORY . $this->fileName;
		}
	}

	public function afterSave() {
		$this->saveFiles();
	}

	private function saveFiles() {
		if ($this->fileTmp) {
			$temp_name = $this->fileTmp->getTempName();
			$directory = self::FILE_DIRECTORY;
			$this->deleteFiles();
			$this->fileName  = $this->newFile($this->fileTmp->getExtensionName());
			$this->fileTmp->saveAs(getcwd() . $directory . $this->fileName);
			$this->isNewRecord = false;
			$this->saveAttributes($this->attributes);
		}
	}

	private function deleteFiles() {
		if ($this->isNewRecord == false) {
			$directory = self::FILE_DIRECTORY;
			if ($this->fileName)   @unlink(getcwd() . $directory . $this->fileName);
		}
	}

	protected function beforeDelete()
	{
		$this->deleteFiles();
		return parent::beforeDelete();
	}

	public function attributeLabels()
	{
		return [
			'name' => Yii::t('documents', 'Имя документа'),
			'active' => Yii::t('documents', 'Активен'),
			'position' => Yii::t('documents', 'Сортировка'),
			'fileTmp' => Yii::t('documents', 'Файл'),
		];
	}

}