<?php

Yii::import('application.models._base.BaseGalleryImage');

use WebPConvert\WebPConvert;

class GalleryImage extends BaseGalleryImage implements LastModified
{
	use LastModifiedModel;

	const IMAGE_DIRECTORY = '/files/gallery/';
	const IMAGE_MAX_SIZE = 52428800;
	const IMAGE_ORIGINAL_WIDTH = 1920;
	const IMAGE_SMALL_WIDTH = 590;
	const IMAGE_EXTENSION = 'jpg';
	
	public $image;
	public $id;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function __construct($s = 'insert')
	{
		if(!file_exists(getcwd() . self::IMAGE_DIRECTORY)) {
			mkdir(getcwd() . self::IMAGE_DIRECTORY, 0777);
		}
		return parent::__construct($s);
	}

	public function rules() {
		return array_merge(parent::rules(), [
			['image', 'file', 'types' => 'jpg, png, gif, jpeg', 'allowEmpty' => true, 'maxSize' => self::IMAGE_MAX_SIZE],
		]);
	}

	private function newImage() {
		return uniqid() . '.' . self::IMAGE_EXTENSION;
	}

	private function newSmallImage() {
		return 'small.'.$this->imageName;
	}

	public function getImage() {
		if ($this->imageName) {
			return self::IMAGE_DIRECTORY . $this->imageName;
		}
	}

	public function getSmallImage() {
		if ($this->imageName) {
			return self::IMAGE_DIRECTORY . 'small.' . $this->imageName;
		}
	}

	public function getImageWebp() {
		if ($this->imageName) {
			if(file_exists(getcwd() . self::IMAGE_DIRECTORY . $this->imageName.'.webp'))
				return self::IMAGE_DIRECTORY . $this->imageName.'.webp';
			else return false;
		}
	}

	public function afterSave() {
		$this->saveFiles();
	}

	private function saveFiles() {
		if ($this->image) {
			$temp_name = $this->image->getTempName();
			$directory = self::IMAGE_DIRECTORY;

			$this->deleteFiles();
			$this->imageName  = $this->newImage();

			list($width, $height) = getimagesize($temp_name);

			$image = new EasyImage($temp_name, 'Imagick');
			$width = $width > self::IMAGE_ORIGINAL_WIDTH ? self::IMAGE_ORIGINAL_WIDTH : $width;

			$image
				->resize($width)
				->save(getcwd() . $directory . $this->imageName, 70);

			$image
				->resize(self::IMAGE_SMALL_WIDTH)
				->save(getcwd() . $directory . $this->newSmallImage(), 70);

			WebPConvert::convert(getcwd() . $directory . $this->imageName, getcwd() . $directory . $this->imageName.'.webp', [
				'quality' => 'auto',
				'max-quality' => 70,
			]);

			WebPConvert::convert(getcwd() . $directory . $this->newSmallImage(), getcwd() . $directory . $this->newSmallImage().'.webp', [
				'quality' => 'auto',
				'max-quality' => 70,
			]);

			$this->isNewRecord = false;
			$this->saveAttributes($this->attributes);
		}
	}

	private function deleteFiles() {
		if ($this->isNewRecord == false) {
			$directory = self::IMAGE_DIRECTORY;
			if ($this->imageName)   {
				@unlink(getcwd() . $directory . $this->imageName.'.webp');
				@unlink(getcwd() . $directory . 'small.' . $this->imageName.'.webp');
				@unlink(getcwd() . $directory . $this->imageName);
				@unlink(getcwd() . $directory . 'small.' . $this->imageName);
			}
		}
	}

	public function deleteImages() {
		if ($this->isNewRecord == false) {
			$this->deleteFiles();
			$this->updateByPk($this->id, [
				'imageName' => null,
			]);
		}
	}

	protected function beforeDelete()
	{
		$this->deleteImages();
		return parent::beforeDelete();
	}

	public function attributeLabels()
	{
		return [
			'image' => Yii::t('gallery', 'Изображение'),
			'active' => Yii::t('gallery', 'Активен'),
			'position' => Yii::t('gallery', 'Сортировка'),
			'description' => Yii::t('gallery', 'Описание'),
			'title' => Yii::t('gallery', 'Заголовок'),
		];
	}

}