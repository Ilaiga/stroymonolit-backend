<?php

Yii::import('application.models._base.BaseServicePricesTable');

class ServicePricesTable extends BaseServicePricesTable
{
	public $id;
	public $position;
	public $depositPrice;
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}