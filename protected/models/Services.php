<?php

Yii::import('application.models._base.BaseServices');

use WebPConvert\WebPConvert;

class Services extends BaseServices implements LastModified
{
	use LastModifiedModel;

	const IMAGE_DIRECTORY = '/files/';
	const IMAGE_MAX_SIZE = 5242880;
	const IMAGE_BG_WIDTH = 1920;
	const IMAGE_PREVIEW_WIDTH = 520;
	const IMAGE_EXTENSION = 'jpg';

	public $parentUrl;
	public $dayPrice;
	public $monthPrice;
	public $halfMonthPrice;
	public $depositPrice;
	public $shortDescription;
	public $advancedDescription;
	public $description;
	public $image;
	public $pImage;
	public $squareMetersPrice;
	public $priceBlockTitle;
	public $formTitle;
	public $requestFormTitle;
	public $shortDescriptionAfterHead;


	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function hasBgImage()
	{
		return empty($this->bgImage) ? true : false;
	}

	public function hasPreviewImage()
	{
		return empty($this->previewImage) ? true : false;
	}

	public function behaviors()
	{
		return array(
			'nestedSetBehavior'=>array(
				'class'=> NestedSetBehavior::class,
				'leftAttribute'=>'lft',
				'rightAttribute'=>'rgt',
				'levelAttribute'=>'level',
				'rootAttribute'=>'root',
				'hasManyRoots'=>true,
			),
		);
	}

	public function rules() {
		return array_merge(parent::rules(), [
			['image, pImage', 'file', 'types' => 'jpg, png, gif, jpeg', 'allowEmpty' => true, 'maxSize' => self::IMAGE_MAX_SIZE],
		]);
	}

	public function relations() {
		return array_merge(parent::relations(),
			[
				'priceItems' => [self::HAS_MANY, 'ServicePricesTable', 'serviceId', 'order' => 'position asc'],
				'sellPriceItems' => [self::HAS_MANY, 'ServiceSellPricesTable', 'serviceId', 'order' => 'position asc'],
				'activePriceItems' => [self::HAS_MANY, 'ServicePricesTable', 'serviceId', 'order' => 'position asc', 'condition' => 'active = 1'],
				'activeSellPriceItems' => [self::HAS_MANY, 'ServiceSellPricesTable', 'serviceId', 'order' => 'position asc', 'condition' => 'active = 1'],
			]);
	}

	public function getPriceTable()
	{
		$head = [
			'Наименование продукции',
			'Цена за сутки',
			'Цена за месяц',
			'Сумма залога',
		];
		foreach ($this->activePriceItems as $item){
			if(empty($item->depositPrice)) unset($head[3]);
			$body[] = [
				$item->name, $item->dayPrice, $item->monthPrice, $item->depositPrice
			];
		}
		$result = [
			'head' => $head,
			'body' => array_map(function (&$a) {if(empty($a[3])) unset($a[3]);return $a;}, $body ?? [])
		];
		return $result;
	}

	public function getSellPriceTable()
	{
		$head = [
			'Наименование продукции',
			'Цена руб. (б/у)',
			'Цена руб. (новые)',
			'Вес, кг',
		];
		$w = false;
		$up = false;
		$np = false;
		foreach ($this->activeSellPriceItems as $item){
			if(!empty($item->weight)) $w = true;
			if(!empty($item->usedPrice)) $up = true;
			if(!empty($item->newPrice)) $np = true;
			$body[] = [
				$item->name, $item->usedPrice ?? null, $item->newPrice ?? null, $item->weight ?? null
			];
		}
		if(!$w) unset($head[3]);
		if(!$up) unset($head[1]);
		if(!$np) unset($head[2]);
		$result = [
			'head' => $head,
			'body' => array_map(function (&$a) {if(empty($a[3])) unset($a[3]);if(empty($a[2])) unset($a[2]);if(empty($a[1])) unset($a[1]);return $a;}, $body ?? [])
		];
		return $result;
	}

	private function newImage() {
		return uniqid() . '.' . self::IMAGE_EXTENSION;
	}

	public function getImage() {
		if ($this->bgImage) {
			return self::IMAGE_DIRECTORY . $this->bgImage;
		}
	}

	public function getPreviewImage() {
		if ($this->previewImage) {
			return self::IMAGE_DIRECTORY . $this->previewImage;
		}
	}

	public function getImageWebp() {
		if ($this->bgImage) {
			if(file_exists(getcwd() . self::IMAGE_DIRECTORY . $this->bgImage.'.webp'))
				return self::IMAGE_DIRECTORY . $this->bgImage.'.webp';
			else return false;
		}
	}

	public function afterSave() {
		$this->saveFiles();
	}

	private function saveFile($img, $imageName, $size) {
		if ($img) {
			$temp_name = $img->getTempName();
			$directory = self::IMAGE_DIRECTORY;
			$this->deleteFiles($imageName);
			$this->$imageName  = $this->newImage();
			list($width, $height) = getimagesize($temp_name);
			$image = new EasyImage($temp_name, 'Imagick');
			$width = $width > $size ? $size : $width;
			$image
				->resize($width)
				->save(getcwd() . $directory . $this->$imageName, 70);
			WebPConvert::convert(getcwd() . $directory . $this->$imageName, getcwd() . $directory . $this->$imageName.'.webp', [
				'quality' => 'auto',
				'max-quality' => 70,
			]);
			$this->isNewRecord = false;
			$this->saveAttributes($this->attributes);
		}
	}

	private function saveFiles() {
		$this->saveFile($this->image, 'bgImage', self::IMAGE_BG_WIDTH);
		$this->saveFile($this->pImage, 'previewImage', self::IMAGE_PREVIEW_WIDTH);
	}

	private function deleteFiles($name) {
		if ($this->isNewRecord == false) {
			$directory = self::IMAGE_DIRECTORY;
			if ($this->$name) {
				@unlink(getcwd() . $directory . $this->$name);
				@unlink(getcwd() . $directory . $this->$name.'.webp');
			}
		}
	}

	public function deleteImages() {
		if ($this->isNewRecord == false) {
			$this->deleteFiles('bgImage');
			$this->deleteFiles('previewImage');
			$this->updateByPk($this->id, [
				'bgImage' => null,
			]);
		}
	}

	public function getUrl()
	{
		foreach ($this->ancestors()->findAll() as $item){
			if($item->slug != '/')
				$url[] = $item->slug;
		}
		$url[] = $this->slug;
		return '/'.implode('/', $url ?? []);
	}

	public function getServicePriceItems()
	{
		$r = [];
		if($this->dayPrice) {
			$r[] = [
				$this->getAttributeLabel('dayPriceTableLabel'),
				$this->dayPrice
			];
		}
		if($this->monthPrice) {
			$r[] = [
				$this->getAttributeLabel('monthPriceTableLabel'),
				$this->monthPrice
			];
		}
		if($this->halfMonthPrice) {
			$r[] = [
				$this->getAttributeLabel('halfMonthPriceTableLabel'),
				$this->halfMonthPrice
			];
		}
		if($this->depositPrice) {
			$r[] = [
				$this->getAttributeLabel('depositPriceTableLabel'),
				$this->depositPrice
			];
		}
		if($this->squareMetersPrice) {
			$r[] = [
				$this->getAttributeLabel('squareMetersPriceTableLabel'),
				$this->squareMetersPrice
			];
		}
		return $r ?? [];
	}

	public function attributeLabels()
	{
		return [
			'title' => Yii::t('pages', 'Заголовок'),
			'menuTitle' => Yii::t('pages', 'Заголовок в меню'),
			'active' => Yii::t('pages', 'Активен'),
			'shortDescription' => Yii::t('pages', 'Краткое описание в шапке'),
			'shortDescriptionAfterHead' => Yii::t('pages', 'Краткое описание после шапки'),
			'advancedDescription' => Yii::t('pages', 'Описание 2'),
			'description' => Yii::t('pages', 'Описание'),
			'dayPrice' => Yii::t('pages', 'Цена за день'),
			'monthPrice' => Yii::t('pages', 'Цена за месяц'),
			'halfMonthPrice' => Yii::t('pages', 'Цена за 15 дней'),
			'depositPrice' => Yii::t('pages', 'Цена залога'),
			'squareMetersPrice' => Yii::t('pages', 'Цена за квадратный метр'),
			'dayPriceTableLabel' => Yii::t('pages', 'День'),
			'monthPriceTableLabel' => Yii::t('pages', 'Месяц'),
			'halfMonthPriceTableLabel' => Yii::t('pages', '15 дней'),
			'depositPriceTableLabel' => Yii::t('pages', 'Залог'),
			'squareMetersPriceTableLabel' => Yii::t('pages', 'Квадратный метр'),
			'image' => Yii::t('pages', 'Изображение'),
			'pImage' => Yii::t('pages', 'Изображение в блоке услуг'),
			'showOnServicesBlock' => Yii::t('pages', 'Показывать в блоке услуг'),
			'showOnServicesBlockOnMainPage' => Yii::t('pages', 'Показывать в блоке услуг на главной'),
			'dontShowOnMenu' => Yii::t('pages', 'Не показывать в меню'),
			'priceBlockTitle' => Yii::t('pages', 'Заголовок блока с ценами'),
			'formTitle' => Yii::t('pages', 'Заголовок формы зявки'),
			'requestFormTitle' => Yii::t('pages', 'Заголовок формы зявки (модальная форма)'),
		];
	}

	public function getType()
	{
		$types = [
			'25' => 'sale',
			'48' => 'rent'
		];
		if($this->level == 2) {
			return $types[$this->id];
		} else {
			return $types[$this->ancestors()->find(new CDbCriteria(['order' => 'rgt', 'condition' => 'level = 2']))->id];
		}
	}

	public function isRent()
	{
		return ($this->getType() == 'rent') ? true : false;
	}

	public static function siteMap(\Tackk\Cartographer\Sitemap $sitemap, $freq = null, $priority = null)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'active = 1';
		$criteria->order = 'updatedAt desc';
		foreach (self::model()->findAll($criteria) as $model) {
			if($model->root == 35) {
				$priority = 0.2;
			} elseif($model->level == 1 || $model->level == 2){
				$priority = 1;
			} elseif($model->level == 3) {
				$priority = 0.8;
			} else {
				$priority = 0.6;
			}
			$sitemap->add(Yii::app()->controller->createAbsoluteUrl($model->getUrl()), $model->updatedAt, $freq, $priority);
		}
	}
}