<?php

Yii::import('application.models._base.BaseRequestForm');

class RequestForm extends BaseRequestForm
{
	public $product, $category, $email, $phone, $comment, $file, $fileName;

	const FILE_DIRECTORY = '/files/';
	const FILE_MAX_SIZE = 2242880;
	const MESSAGE_HEADER = [
		'Ваша заявка принята',
		'Спасибо за оформление подписки'
	];
	const MESSAGE = [
		'Наш менеджер свяжется с вами в ближайшее время для уточнения деталей заказа и профессионального консультирования.',
		''
	];

	public function rules() {
		return array_merge(parent::rules(), [
			['file', 'file', 'types' => 'rar, zip, doc, xls, txt, gif, jpg, png, bmp, docx, pdf, xlsx', 'allowEmpty' => true, 'maxSize' => self::FILE_MAX_SIZE],
		]);
	}

	private function newFile($ext) {
		return uniqid() . '.' . $ext;
	}

	public function getFile() {
		if ($this->fileName) {
			return self::FILE_DIRECTORY . $this->fileName;
		}
	}

//	public function beforeSave()
//	{
//		$this->createDate = date("Y-m-d H:i:s");
//		parent::beforeSave();
//	}

	public function afterSave() {
		$this->saveFiles();
		$subscribers = array_filter(explode(',', \Yii::app()->settings->get('siteSettings:organization:subscribers')), 'trim');
		if(empty($subscribers)) return true;
		$mail = \Yii::app()->mailer;
		$mail->Subject = 'Поступила новая заявка';
		$mail->SetFrom(\Yii::app()->settings->get('siteSettings:organization:email'),  str_replace('https://', '', \Yii::app()->getBaseUrl(true)));
		$mail->MsgHTML($mail->getView('requests/new', ['data' => $this]));
		foreach ($subscribers as $email) {
			$mail->AddAddress($email);
		}
		$mail->Send();
		parent::afterSave();
	}

	private function saveFiles() {
		if ($this->file) {
			$temp_name = $this->file->getTempName();
			$directory = self::FILE_DIRECTORY;
			$this->deleteFiles();
			$this->fileName  = $this->newFile($this->file->getExtensionName());
			$this->file->saveAs(getcwd() . $directory . $this->fileName);
			$this->isNewRecord = false;
			$this->saveAttributes($this->attributes);
		}
	}

	private function deleteFiles() {
		if ($this->isNewRecord == false) {
			$directory = self::FILE_DIRECTORY;
			if ($this->fileName)   @unlink(getcwd() . $directory . $this->fileName);
		}
	}

	protected function beforeDelete()
	{
		$this->deleteFiles();
		return parent::beforeDelete();
	}
	
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}