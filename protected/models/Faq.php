<?php

Yii::import('application.models._base.BaseFaq');

class Faq extends BaseFaq implements LastModified
{
	use LastModifiedModel;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function attributeLabels()
	{
		return [
			'question' => Yii::t('faq', 'Вопрос'),
			'active' => Yii::t('faq', 'Активен'),
			'position' => Yii::t('faq', 'Сортировка'),
			'answer' => Yii::t('faq', 'Ответ'),
		];
	}

}