<?php

Yii::import('application.models._base.BaseArticles');

use WebPConvert\WebPConvert;

class Articles extends BaseArticles implements LastModified
{
	use LastModifiedModel;

	const IMAGE_DIRECTORY = '/files/';
	const IMAGE_MAX_SIZE = 5242880;
	const IMAGE_BIG_WIDTH = 969;
	const IMAGE_SMALL_WIDTH = 590;
	const IMAGE_EXTENSION = 'jpg';

	public $image;
	public $metaTitle;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return array_merge(parent::rules(), [
			['slug, name, metaTitle, metaDescription', 'filter', 'filter' => 'strip_tags'],
			['slug, name, metaTitle, metaDescription, text', 'filter', 'filter' => 'trim'],
			['slug', 'unique', 'allowEmpty' => false, 'attributeName' => 'slug'],
			['image', 'file', 'types' => 'jpg, png, gif, jpeg', 'allowEmpty' => true, 'maxSize' => self::IMAGE_MAX_SIZE],
		]);
	}

	public function publicSearch() {
		$criteria = new CDbCriteria();
		$criteria->limit = 100;
		$criteria->offset = 0;
		$criteria->order = 'articleID desc';
		$pagination = [
			'pageSize' => 100,
			'pageVar' => 'page'
		];

		return new CActiveDataProvider($this, [
			'criteria' => $criteria,
			'pagination' => $pagination
		]);
	}

	public function allSearch() {
		$criteria = new CDbCriteria();
		$criteria->order = 't.articleID DESC';
		$criteria->with = ['category'];
		
		$pagination = [
			'pageSize' => 20,
			'pageVar'  => 'page'
		];
		
		return new CActiveDataProvider($this, [
			'criteria' => $criteria,
			'pagination' => $pagination
		]);
	}

	public function getCreated() {
		return Yii::app()->dateFormatter->format('d MMMM yyyy', $this->created);
	}

	public function getReadingTime() {
		$plurals = [
			'{n} минута на чтение',
			'{n} минуты на чтение',
			'{n} минут на чтение',
			'{n} минуты на чтение'
		];
		
		return Yii::t('app', implode('|', $plurals), $this->readingTime);
	}

	public function getNext() {
		$result = Yii::app()->db
			->createCommand("SELECT * FROM `{$this->tableName()}` WHERE `articleID` = (SELECT MIN(`articleID`) FROM `{$this->tableName()}` WHERE `articleID` > {$this->articleID})")
			->queryRow();
		
		return $result;
	}

	public function getPrev() {
		$result = Yii::app()->db
			->createCommand("SELECT * FROM `{$this->tableName()}` WHERE `articleID` = (SELECT MAX(`articleID`) FROM `{$this->tableName()}` WHERE `articleID` < {$this->articleID})")
			->queryRow();

		return $result;
	}

	public function viewsUp() {
		Yii::app()->db
			->createCommand("UPDATE `{$this->tableName()}` SET `views` = `views` + 1 WHERE `articleID` = {$this->articleID}")
			->execute();
	}

	private function newImageBig() {
		return uniqid() . '.big.' . self::IMAGE_EXTENSION;
	}

	public function getImageBig() {
		if ($this->imageBig) {
			return self::IMAGE_DIRECTORY . $this->imageBig;
		}
	}

	private function newImageSmall() {
		return uniqid() . '.small.' . self::IMAGE_EXTENSION;
	}

	public function getImageSmall() {
		if ($this->imageSmall) {
			return self::IMAGE_DIRECTORY . $this->imageSmall;
		}
	}

	// events
	public function afterSave() {
		$this->saveFiles();
	}
	
	public function beforeDelete() {
		$this->deleteFiles();
		return parent::beforeDelete();
	}
	
	public function beforeValidate() {
		$this->slug = ('' == $this->slug) ? $this->name : $this->slug;
		$this->slug = Utilities::transliterate($this->slug);
		$this->categoryID = 1;
		return parent::beforeValidate();
	}

	public function afterFind()
	{
		if(empty($this->metaDescription)) {
			$this->metaDescription =  'Читайте подробную статью на сайте Московской Монолитной Компании: '.$this->name.'';
		}
		parent::afterFind();
	}

	// image upload & delete
	private function saveFiles() {
		if ($this->image) {
			$temp_name = $this->image->getTempName();
			$directory = self::IMAGE_DIRECTORY;

			$this->deleteFiles();
			$this->imageBig   = $this->newImageBig();
			$this->imageSmall = $this->newImageSmall();

			list($width, $height) = getimagesize($temp_name);

			$image = new EasyImage($temp_name, 'Imagick');
			$width = $width > self::IMAGE_BIG_WIDTH ? self::IMAGE_BIG_WIDTH : $width;

			$image
				->resize($width)
				->save(getcwd() . $directory . $this->imageBig, 70);
			$image
				->resize(self::IMAGE_SMALL_WIDTH)
				->save(getcwd() . $directory . $this->imageSmall, 70);


			WebPConvert::convert(getcwd() . $directory . $this->imageBig, getcwd() . $directory . $this->imageSmall.'.webp', [
				'quality' => 'auto',
				'max-quality' => 80,
			]);


			$this->isNewRecord = false;
			$this->saveAttributes($this->attributes);
		}
	}

	private function deleteFiles() {
		if ($this->isNewRecord == false) {
			$directory = self::IMAGE_DIRECTORY;
			if ($this->imageBig)   @unlink(getcwd() . $directory . $this->imageBig);
			if ($this->imageSmall) @unlink(getcwd() . $directory . $this->imageSmall);
			if ($this->imageBig)   @unlink(getcwd() . $directory . $this->imageBig.'.webp');
			if ($this->imageSmall) @unlink(getcwd() . $directory . $this->imageSmall.'.webp');
		}
	}

	public function deleteImages() {
		if ($this->isNewRecord == false) {
			$this->deleteFiles();
			$this->updateByPk($this->articleID, [
				'imageBig' => null,
				'imageSmall' => null
			]);
		}
	}

	public function isWritable() {
		$dir = getcwd() . self::IMAGE_DIRECTORY;
		return is_writable($dir);
	}

	public static function getWidgetNews()
	{
		$criteria = new CDbCriteria();
		$criteria->limit = 3;
		$criteria->offset = 0;
		$criteria->order = 'created desc';
		return Articles::model()->findAll($criteria);
	}

	public function attributeLabels()
	{
		return [
			'active' => Yii::t('articles', 'Активен'),
			'name' => Yii::t('articles', 'Заголовок'),
			'created' => Yii::t('articles', 'Дата создания'),
			'text' => Yii::t('articles', 'Текст'),
			'shortText' => Yii::t('articles', 'Краткий текст'),
			'image' => Yii::t('articles', 'Изображение'),
			'readingTime' => Yii::t('articles', 'Время на чтение'),
		];
	}

	public static function siteMap(\Tackk\Cartographer\Sitemap $sitemap, $freq = null, $priority = null)
	{
		$criteria = new CDbCriteria();
		$criteria->order = 'updatedAt desc';
		foreach (self::model()->findAll($criteria) as $model) {
			$sitemap->add(Yii::app()->controller->createAbsoluteUrl('articles/view', ['slug' => $model->slug]), $model->updatedAt, $freq, $priority);
		}
	}

	public function getInterest($limit = 3)
	{
		$criteria = new CDbCriteria();
		$criteria->select = '*, rand() as rand';
		$criteria->order = 'rand';
		$criteria->limit = 3;
		return self::model()->findAll($criteria) ?? [];
	}

	public function getPrevArticle()
	{
		$criteria = new CDbCriteria();
		$criteria->order = 'articleID desc';
		$criteria->condition = 'articleID < :id';
		$criteria->params = [':id' => $this->articleID];

		return self::model()->find($criteria) ?? [];
	}

	public function getNextArticle()
	{
		return self::model()->findByAttributes([], 'articleID > :id', [':id' => $this->articleID]) ?? [];
	}

	public function getUrl()
	{
		return Yii::app()->controller->createUrl('/articles/view', ['slug' => $this->slug]);
	}
}