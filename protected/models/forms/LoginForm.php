<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 30.04.2019
 * Time: 16:39
 */

class LoginForm extends CModel
{
	public  $login;
	public  $password;
	public  $name = 'admin';
	public  $role = 'admin';

	public function attributeNames()
	{
		return ['login', 'password'];
	}

	public function rules() {
		return array(
			array('login, password', 'required'),
		);
	}
}