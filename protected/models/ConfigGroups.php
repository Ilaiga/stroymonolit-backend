<?php

Yii::import('application.models._base.BaseConfigGroups');

class ConfigGroups extends BaseConfigGroups
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function behaviors() {
		return array(
			'tree' => array(
				'class' => 'ActiveRecordTreeBehavior',
				'order' => 'parentID DESC',
				'idParentField' => 'parentID',
				'with' => 'configParams',
			),
		);
	}
}