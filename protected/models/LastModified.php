<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 14.08.2019
 * Time: 10:41
 */

interface LastModified
{
	public function getLastModifiedDate() : DateTime;
}