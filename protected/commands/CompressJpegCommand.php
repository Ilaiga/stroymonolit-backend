<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 17.09.2019
 * Time: 15:42
 */

class CompressJpegCommand extends CConsoleCommand
{
	protected $compressExt = ['jpg', 'jpeg'];

	public function run($args) {
		$this->compress(getcwd() . '/..' . '/files/');
		$this->compress(getcwd() . '/..' . '/img/');
		echo "Done".PHP_EOL;
	}

	function compress($dir) {
		$res = $this->scanAllDir($dir);
		foreach ($res as $file) {
			$fa = explode('.', $file);
			$ext = array_pop($fa);
			if(in_array($ext, $this->compressExt)) {
				$image = new EasyImage($dir.$file, 'Imagick');
				$image->save($dir.$file, 70);
				echo $file. ' ' .$ext.PHP_EOL;
			}

		}
	}

	function scanAllDir($dir) {
		$result = [];
		foreach(scandir($dir) as $filename) {
			if ($filename[0] === '.') continue;
			$filePath = $dir . '/' . $filename;
			if (is_dir($filePath)) {
				foreach ($this->scanAllDir($filePath) as $childFilename) {
					$result[] = $filename . '/' . $childFilename;
				}
			} else {
				$result[] = $filename;
			}
		}
		return $result;
	}
}