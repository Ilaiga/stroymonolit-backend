<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 30.04.2019
 * Time: 13:23
 */
use WebPConvert\WebPConvert;

class WebpCommand extends CConsoleCommand
{
	protected $compressExt = ['jpg', 'jpeg', 'png'];

	public function run($args) {
		$this->compress(getcwd() . '/..' . '/files/');
		$this->compress(getcwd() . '/..' . '/images/');
		echo "Done".PHP_EOL;
	}

	function compress($dir) {
		$res = $this->scanAllDir($dir);
		foreach ($res as $file) {
			$fa = explode('.', $file);
			$ext = array_pop($fa);
			if(in_array($ext, $this->compressExt) && !file_exists($dir.$file.'.webp')) {
				WebPConvert::convert($dir.$file, $dir.$file.'.webp', [
					'quality' => 'auto',
					'max-quality' => 80,
				]);
				echo $file. ' ' .$ext.PHP_EOL;
			}

		}
	}

	function scanAllDir($dir) {
		$result = [];
		foreach(scandir($dir) as $filename) {
			if ($filename[0] === '.') continue;
			$filePath = $dir . '/' . $filename;
			if (is_dir($filePath)) {
				foreach ($this->scanAllDir($filePath) as $childFilename) {
					$result[] = $filename . '/' . $childFilename;
				}
			} else {
				$result[] = $filename;
			}
		}
		return $result;
	}
}

