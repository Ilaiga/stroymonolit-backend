<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 20.06.2019
 * Time: 14:39
 */

class DocumentsController extends Controller
{
	public function actionIndex()
	{
		$this->setLastModifiedModels(Documents::model());
		$this->render('index', ['documents' => Documents::model()->findAllByAttributes(['active' => 1], ['order' => 'position asc'])]);
	}
}