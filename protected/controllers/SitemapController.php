<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 14.08.2019
 * Time: 15:18
 */

use Tackk\Cartographer\Sitemap;
use Tackk\Cartographer\ChangeFrequency;

class SitemapController extends Controller
{
	public function actionIndex()
	{
		$sitemap = new Tackk\Cartographer\Sitemap();
		Articles::siteMap($sitemap, ChangeFrequency::WEEKLY, 0.4);
		Services::siteMap($sitemap, ChangeFrequency::WEEKLY);
		Pages::siteMap($sitemap, ChangeFrequency::WEEKLY, 0.2);
		header ('Content-Type:text/xml');
		echo $sitemap->toString();
	}
}