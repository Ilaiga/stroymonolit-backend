<?php

class PageController extends Controller
{
	protected $page;

	public function actionView($pageId)
	{
		$page = Pages::model()->findByPk($pageId);
		$this->setPageDescription($page->description);
		$this->setPageTitle($page->metaTitle);
		$this->setLastModified($page);
		$this->render($page->template->name, ['page' => $page]);
	}

	public function getPageId()
	{
		$page = $this->page->parent()->find();
		if($page->slug=='/')
			return $this->page->slug;
		else return $page->slug;
	}

	public function getCurrentPageId()
	{
		return $this->page->slug;
	}
}