<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 19.06.2019
 * Time: 12:24
 */

class CallbackController extends Controller
{
	public function actionIndex()
	{
		$oRequest = new RequestForm();
		if (Yii::app()->request->isPostRequest) {
			$data = Yii::app()->request->getPost('CallbackForm');
			$oRequest->setAttributes($data);
			$oRequest->file = UploadedFile::getInstancesByName('CallbackForm[file]');
			$captcha = new GoogleRecaptcha(Yii::app()->request->getPost('g-recaptcha-response'));
			$captcha_error = [];
			if($oRequest->validate()) {
				if($captcha->getResult()) {
					$oRequest->save(false);
					Yii::app()->user->setFlash('message_header', $oRequest::MESSAGE_HEADER[0]);
					Yii::app()->user->setFlash('message', $oRequest::MESSAGE[0]);
					if(Yii::app()->request->isAjaxRequest) {
						echo 'ok'; die;
					}
					else $this->redirect('/message');
				} else {
					$captcha_error['captcha'] = 'error';
				}
			}
			$error = array_merge($oRequest->getErrors(), $captcha_error);
			echo json_encode(['errors' => $error]);
		}
	}
}