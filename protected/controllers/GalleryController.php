<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 20.06.2019
 * Time: 15:05
 */

class GalleryController extends Controller
{
	public function actionIndex()
	{
		$this->setLastModifiedModels(GalleryImage::model());
		$this->render('index', ['images' => GalleryImage::model()->findAllByAttributes(['active' => 1], ['order' => 'position asc'])]);
	}
}