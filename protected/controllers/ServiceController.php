<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 18.06.2019
 * Time: 17:55
 */

class ServiceController extends Controller
{
	protected $page;
	protected $templates = [
		'35' => 'brand',
		'1' => 'service'
	];

	protected $customTemplates = [
		'48' => 'base_service',
		'25' => 'base_service',
	];

	public function actionView($pageId)
	{
		$page = Services::model()->findByPk($pageId);
		$this->setLastModified($page);
		$this->page = $page;
		$this->setPageTitle($page->metaTitle);
		$this->setPageDescription($page->metaDescription);
		$this->render(!empty($this->customTemplates[$page->id]) ? $this->customTemplates[$page->id] : $this->templates[$page->root], ['service' => $page]);
	}

	public function getPageId()
	{
		$page = $this->page->parent()->find();
		if($page->slug=='/')
			return $this->page->slug;
		else return $page->slug;
	}

	public function getCurrentPageId()
	{
		return $this->page->slug;
	}
}