<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 19.06.2019
 * Time: 17:30
 */

class FaqController extends Controller
{
	public function actionIndex()
	{
		$this->setLastModifiedModels(Faq::model());
		$this->render('index', ['questions' => Faq::model()->findAllByAttributes(['active' => 1], ['order' => 'position asc'])]);;
	}
}