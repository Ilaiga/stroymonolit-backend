<?php

class SiteController extends Controller
{
	public function actionIndex()
	{
        $this->render('index', []);//'articles' => Articles::getWidgetNews()
	}

	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				// назначение тайтлов нужно будет вывести в twig
				switch($error['code']) {
					case 403: Yii::app()->controller->pageTitle = 'Ошибка доступа'; break;
					case 404: Yii::app()->controller->pageTitle = 'Страница недоступна'; break;
					case 500: Yii::app()->controller->pageTitle = 'Проблема с сервером'; break;
					default:  Yii::app()->controller->pageTitle = 'Неизвестная ошибка'; break;
				}
				$this->render('error', $error);
			}
		}
	}

	public function actionHttp($code)
	{
		throw new CHttpException($code);
	}

}