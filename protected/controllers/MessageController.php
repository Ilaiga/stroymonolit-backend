<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 02.07.2019
 * Time: 12:51
 */

class MessageController extends Controller
{
	public function actionIndex()
	{
		$this->setPageTitle('Ваша заявка принята');
		if(!Yii::app()->user->hasFlash('message')) $this->redirect('/');
		$this->render('index');
	}
}