<?php

class ArticlesController extends Controller
{
	public $name = 'Статьи';

	public function actionIndex($category = null) {
		$this->setPageTitle($this->name);
		$this->setLastModifiedModels(Articles::model());
		$this->pageDescription = "descr";
		$articles = new Articles('search');
		$this->render('index', [
			'articles' => $articles,
			'provider' => $articles->publicSearch(),
		]);
	}
	
	public function actionView($slug) {
		$article = Articles::model()->findByAttributes(['slug' => $slug]);
		if(empty($article)) {
			throw new CHttpException('404', 'Article not found');
		}
		$this->setLastModified($article);
		$article->viewsUp();
		Yii::app()->controller->pageTitle = $article->metaTitle;
		$this->pageDescription = $article->metaDescription;
		$this->render('view', [
			'article' => $article
		]);
	}
}