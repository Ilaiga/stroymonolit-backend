<?php

class Utilities
{
	public static function transliterate($string) {
		$rus = array(
			'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo',
			'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm',
			'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u',
			'ф' => 'f', 'х' => 'h', 'ц' => 'ts', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh', 'ъ' => '',
			'ы' => 'i', 'ь' => '', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya'
		);
		$string = preg_replace("/[^A-zА-я0-9\-\s]/u", "", $string);
		$string = trim($string);
		$string = str_replace(" ", "-", $string);
		$string = (function_exists("mb_strtolower") ? mb_strtolower($string, "utf-8") : $string);
		$string = strtr($string, $rus);
		return $string;
	}
}