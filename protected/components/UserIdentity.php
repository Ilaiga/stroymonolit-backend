<?php

use \application\models\User;
/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    private $id;
    protected $dummyUser = [
    	'login' => 'admin',
	    'password' => 'Reppass123z',
//	    'password' => 'admin',
    ];

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     * @throws CException
     */
	public function authenticate()
	{
//        print_r($this->username);die;
		if(empty($this->username))
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		elseif($this->password != $this->dummyUser['password'])
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else {
			$this->id = 1;
            $this->setState('title', 'admin');
            $this->errorCode=self::ERROR_NONE;
        }
		return !$this->errorCode;
	}

	public function getId()
	{
		return $this->id;
	}

}