<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 18.06.2019
 * Time: 17:19
 */

class ServicesUrlRule extends  CBaseUrlRule
{
	public $connectionID;

	public function createUrl($manager, $route, $params, $ampersand) {
		return false;
	}

	public function parseUrl($manager, $request, $pathInfo, $rawPathInfo) {
		if(empty($rawPathInfo)) {
			$_GET['pageId'] = 1;
			$_GET['routePage'] = true;
		}
		$parts  = explode('/', $rawPathInfo);
		$slug = trim(array_pop($parts));
		$pages = Services::model()->findAll('slug=:slug', [':slug' => $slug]);
		foreach ($pages as $page) {
			if($this->getParentUrl($page) === '/' . $rawPathInfo && $page->active) {
				$_GET['pageId'] = $page->id;
				if(!empty($page->route)) {
					$_GET['routePage'] = true;
					return $page->route;
				}
				return 'service/view';
			}
		}
		return false;
	}

	private function getParentUrl(Services $page)
	{
		$ancestors = $page->ancestors()->findAll();
		$url = [];
		foreach ($ancestors as $parentPage) {
			if($parentPage->slug === '/') continue;
			$url[] = $parentPage->slug;
		}
		$url[] = $page->slug;
		return '/' . implode('/', $url);
	}
}