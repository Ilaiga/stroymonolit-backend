<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	public $pageDescription;

	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	protected $headers = [];

	protected function beforeAction($action) {
		$this->setPageTitle();
		$this->setPageDescription();
		return parent::beforeAction($action);
	}

	public function setPageTitle($title = '') {
		if(empty($title)) {
			if($this->getPage()) {
				return $this->pageTitle = $this->getPage()->metaTitle;
			}
		}
		return $this->pageTitle = $title;
	}

	public function setPageDescription($title = '') {
		if(empty($title)) {
			if($this->getPage()) {
				return $this->pageDescription = $this->getPage()->metaDescription;
			}
		}
		return $this->pageDescription = $title;
	}

	public function getPage()
	{
		if(($pageId = Yii::app()->request->getParam('pageId'))) {
			return Pages::model()->findByPk($pageId);
		}
	}

	public function getMainPriceTable()
	{
		foreach (ServicePricesTable::model()->findAllByAttributes(['showOnMainPage' => 1], ['order' => 'dayPrice asc']) as $item){
			$result[] = [
				$item->name, $item->dayPrice, $item->monthPrice
			];
		}
		return $result ?? [];
	}

	public function getReviews()
	{
		return Reviews::model()->findAllByAttributes(['active' => 1], ['order' =>'position asc']);
	}

	public function getServicesBlock(Services $baseService = null) {
		$excudeId = $baseService ? $baseService->id : 1;
		$services = $baseService
			? $baseService->parent()->find()
			: Services::model()->findByAttributes(['id' => 1]);
		if($services->id == 1) $services = $baseService;
		$services = $services->descendants()->findAllByAttributes(['showOnServicesBlock' => 1], 'id != :id', [':id' => $excudeId]);
		foreach ($services as $service) {
			$dPrice = $service->dayPrice ? '' . $service->dayPrice . ' ₽ / сутки' : '';
			$mPrice = $service->squareMetersPrice ? $service->squareMetersPrice . ' ₽ за м<sup>2</sup>' : '';
			$result[] = [
				'img' => $service->getPreviewImage(),
				'title' => $service->title,
				'price' => $mPrice ? $mPrice : $dPrice,
				'type' => $service->parent()->find()->title,
				'link' => $service->getUrl()
			];
		}
		return $result ?? [];
	}

	public function getRentServicesBlock() {
		$services = Services::model()->findByAttributes(['id' => 48]);
		$services = $services->descendants()->findAllByAttributes(['showOnServicesBlock' => 1]);
		foreach ($services as $service) {
			$dPrice = $service->dayPrice ? '' . $service->dayPrice . ' ₽ / сутки' : '';
			$mPrice = $service->squareMetersPrice ? $service->squareMetersPrice . ' ₽ за м<sup>2</sup>' : '';
			$result[] = [
				'img' => $service->getPreviewImage(),
				'title' => $service->title,
				'price' => $mPrice ? $mPrice : $dPrice,
				'type' => $service->parent()->find()->title,
				'link' => $service->getUrl()
			];
		}
		return $result ?? [];
	}

	public function getServicesBlockOnMainPage(Services $baseService = null) {
		$excudeId = $baseService ? $baseService->id : 1;
		$services = $baseService
			? $baseService->parent()->find()
			: Services::model()->findByAttributes(['id' => 1]);
		$services = $services->descendants()->findAllByAttributes(['showOnServicesBlockOnMainPage' => 1], 'id != :id', [':id' => $excudeId]);
		foreach ($services as $service) {
			$dayPrice = $service->dayPrice ? 'от ' . $service->dayPrice . ' ₽ / сутки' : '';
			$m2Price = $service->squareMetersPrice ? 'от ' . $service->squareMetersPrice . ' ₽ / м<sup>2</sup>' : '';
			$result[] = [
				'img' => $service->getPreviewImage(),
				'title' => $service->title,
				'price' => $dayPrice ? $dayPrice : $m2Price,
				'type' => $service->parent()->find()->title,
				'link' => $service->getUrl()
			];
		}
		return $result ?? [];
	}

	public function getPageId()
	{
		return $this->getId();
	}

	public function getCurrentPageId()
	{
		return $this->getId();
	}

	public function getMainMenu()
	{
		$category=Pages::model()->findByAttributes(['id' => 1]);
		return $category->children()->findAll(new CDbCriteria(['condition' => 'active = 1 and id not in(16, 26, 27, 28)']));
	}

	public function getMainMenuFooter()
	{
		$category=Pages::model()->findByAttributes(['id' => 1]);
		return $category->children()->findAll(new CDbCriteria(['condition' => 'active = 1 and id not in(26, 28)']));
	}

	public function getLeftServiceMenu()
	{
		$category=Services::model()->findByAttributes(['id' => 1]);
		$ch = $category->children()->findAllByAttributes(['active' => 1, 'dontShowOnMenu' => 0]);
		foreach ($ch as $cat) {
			$r = null;
			$r['text'] = $cat->title;
			$r['url'] = $cat->getUrl();
			$r['class'] = $cat->getUrl() == Yii::app()->request->requestUri ? '_active' : '';
			$ch2 = $cat->children()->findAllByAttributes(['active' => 1, 'dontShowOnMenu' => 0]);
			foreach ($ch2 as $serv) {
				foreach ($serv->children()->findAllByAttributes(['active' => 1, 'dontShowOnMenu' => 0]) as $serv2) {
					$subItems[] = [
						'text' => $serv2['menuTitle'],
						'link' => Yii::app()->request->requestUri == $serv2->getUrl() ? 'javascript:;' : $serv2->getUrl(),
						'class' => Yii::app()->request->requestUri == $serv2->getUrl() ? '_active' : '',
					];
				}
				$r['subList'][] = [
					'text' => $serv['menuTitle'],
					'link' => Yii::app()->request->requestUri == $serv->getUrl() ? 'javascript:;' : $serv->getUrl(),
					'class' => Yii::app()->request->requestUri == $serv->getUrl() ? '_active' : '',
					'subList' => $subItems ?? []
				];
				unset($subItems);
			}
			$result[] = $r;
		}
		return $result ?? [];
	}

	public function setBreadCrumbs(array $items = []) {
		return $this->breadcrumbs = $items;
	}
	
	protected function loadRecord($class_name, $pk = 'id') {
		if (class_exists($class_name)) {
			if ($id = (int) Yii::app()->request->getParam($pk)) {
				$record = $class_name::model()->findByPk($id);
			}
		}
		if (!isset($record) || NULL === $record) {
			throw new CHttpException(404, 'Record not found.');
		}
		return $record;
	}
	
	protected function loadRecordByAttribute($class_name, $params) {
		if (class_exists($class_name)) {
			$record = $class_name::model()->findByAttributes($params);
		}
		if (!isset($record) || NULL === $record) {
			throw new CHttpException(404, 'Record not found.');
		}
		return $record;
	}

	public function render($view, $data=null, $return=false) {
		if(!empty($_GET['routePage'])) {
			$page = Pages::model()->findByPk(intval($_GET['pageId']));
			if($page) {
				$this->setPageTitle($page->metaTitle);
				$this->setPageDescription($page->description);
				$data = $data ?? [];
				$data = array_merge($data, ['page' => $page]);
			}
		}
		parent::render($view, $data, $return);
	}

	public function setLastModified(LastModified $model)
	{
//		if(!empty(getallheaders()['If-Modified-Since'])) {
//			$ifModifiedSince = new DateTime(getallheaders()['If-Modified-Since']);
//			if($ifModifiedSince >= $model->getLastModifiedDate()) {
//				header ($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
//				exit;
//			}
//		}
//		$this->headers[] = 'Last-Modified: '.gmdate('D, d M Y H:i:s', $model->getLastModifiedDate()->getTimestamp()).' GMT';
	}

	public function setLastModifiedModels(LastModified $model)
	{
		$criteria = new CDbCriteria([
			'order' => 'updatedAt desc'
		]);
		$this->setLastModified($model->find($criteria));
	}

	protected function beforeRender($view)
	{
		$this->setHeaders();
		Yii::app()->openGraph->title($this->pageTitle)->description($this->pageDescription)->url()->type('page');
		return parent::beforeRender($view);
	}

	public function setHeaders()
	{
		foreach ($this->headers as $header) {
			header($header);
		}
	}
}