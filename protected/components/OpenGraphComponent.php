<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 16.08.2019
 * Time: 11:24
 */

class OpenGraphComponent extends CApplicationComponent
{
	const DEFAULT_IMAGE = '/img/index/intro-bg.png';

	protected $output;

	public function title($title)
	{
		$title = $title ?? Yii::app()->controller->getPageTitle();
		$this->output[__FUNCTION__] = "<meta property=\"og:title\" content=\"$title\"/>";
		return $this;
	}

	public function url($url = null)
	{
		$url = $url ?? Yii::app()->controller->createAbsoluteUrl(Yii::app()->request->requestUri);
		$this->output[__FUNCTION__] = "<meta property=\"og:url\" content= \"$url\" />";
		return $this;
	}

	public function description($description)
	{
		$description = strip_tags($description);
		$this->output[__FUNCTION__] = "<meta property=\"og:description\" content=\"$description\"/>";
		return $this;
	}

	public function image($image)
	{
		$image = Yii::app()->controller->createAbsoluteUrl($image);
		$this->output[__FUNCTION__] = "<meta property=\"og:image\" content=\"$image\"/>";
		return $this;
	}

	public function type($type)
	{
		$this->output[__FUNCTION__] = "<meta property=\"og:type\" content=\"$type\"/>";
		return $this;
	}

	public function __toString()
	{
		$this->setDefaultImage();
		return implode($this->output);
	}

	protected function setDefaultImage()
	{
		if(empty($this->output['image'])) {
			$this->image(self::DEFAULT_IMAGE);
		}
	}
}