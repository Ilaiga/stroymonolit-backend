$(document).ready(function(){
	let $element;
	$('#nestable3').nestable({
		beforeDragStop: function(l,e, p){
			$element = e;
		}
	}).on('change', function () {
		let action;
		let id = $element.attr('data-id');
		let targetId;
		if($element.next().attr('data-id')) {
			targetId = $element.next().attr('data-id');
			action = 'before';
		} else if($element.prev().attr('data-id')) {
			targetId = $element.prev().attr('data-id');
			action = 'after';
		} else if($element.closest('ol').closest('li').attr('data-id')) {
			targetId = $element.closest('ol').closest('li').attr('data-id');
			action = 'append';
		}
		if(action && id && targetId) {
			let data = {'pos':{'id': id, 'targetId': targetId, 'action': action}};
			$.ajax({
				url: '/admin/pages/changePosition/',
				method: 'post',
				data: data,
				dataType: 'json',
				success: function (data) {
					console.log(data);
				}
			});
		}
	});
	let $element2;
	$('#nestable2').nestable({
		beforeDragStop: function(l,e, p){
			$element2 = e;
		}
	}).on('change', function () {
		let action;
		let id = $element2.attr('data-id');
		let targetId;
		if($element2.next().attr('data-id')) {
			targetId = $element2.next().attr('data-id');
			action = 'before';
		} else if($element2.prev().attr('data-id')) {
			targetId = $element2.prev().attr('data-id');
			action = 'after';
		} else if($element2.closest('ol').closest('li').attr('data-id')) {
			targetId = $element2.closest('ol').closest('li').attr('data-id');
			action = 'append';
		}
		if(action && id && targetId) {
			let data = {'pos':{'id': id, 'targetId': targetId, 'action': action}};
			$.ajax({
				url: '/admin/services/changePosition/',
				method: 'post',
				data: data,
				dataType: 'json',
				success: function (data) {
					console.log(data);
				}
			});
		}
	});
});