$(document).ready(function () {

	$(document).on('click', '.js-add-price-item', function(){
		let pricesBox = $(this).closest('.box').find('tbody.js-prices-box');
		let tmp = $(this).closest('.box').find('.price-tmp.hidden').clone().removeClass('price-tmp hidden');
		let i = Math.round(Math.random()*100000, 0);
		tmp.find('input').each(function(item){
			$(this).attr('name', $(this).attr('name').replace('#i#', i));
		});
		pricesBox.append(tmp);
	});
	$(document).on('click', '.js-delete-price-item', function() {
		if(!confirm('Вы действительно хотите удалить запись?')) return false;
		$(this).closest('tr').remove();
		return false;
	});
	$(document).on('click', '.js-prices-save-button', function() {
		$('tr.hidden').remove();
	});
});